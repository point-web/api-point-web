# SDK utilisé pour communiquer avec l'API node Point-Web

Exemple d'utilisation du SDK

```
$clientBuilder = new ClientBuilder();
$clientBuilder->addPlugin(new HeaderDefaultsPlugin([
'Accept' => 'application/json',
]));

$sdk = new Sdk($clientBuilder);
$response = $sdk->blacklist()->get();
```