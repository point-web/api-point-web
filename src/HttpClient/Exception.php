<?php

declare(strict_types=1);

namespace Api\Client\HttpClient;

class Exception extends \Exception
{
    public function __toString() {
        return $this->toString();
    }
    
    public function toString() {
        return "{$this->getCode()} | {$this->getMessage()}";
    }
}
