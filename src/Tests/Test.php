<?php

require_once __DIR__ . '/../vendor/autoload.php';

$clientBuilder = new \Api\Client\ClientBuilder();
$clientBuilder->addPlugin(new \Http\Client\Common\Plugin\HeaderDefaultsPlugin([
    'Accept' => 'application/json',
    'apiKey' => 'YOUR_API_KEY',
]));

$sdk = new \Api\Client\Sdk($clientBuilder);
try {
    //Add SPAM
    $response = $sdk->blacklist()->add("te@te.fr");
    //Get specific address
    $response = $sdk->blacklist()->get("te@te.fr");
    //Get alla adresses
    $response = $sdk->blacklist()->get();
} catch (\Api\Client\HttpClient\Exception|Exception $exception) {
    if($exception instanceof \Api\Client\HttpClient\Exception)
        die(\Symfony\Component\VarDumper\VarDumper::dump($exception->toString()));
    else // Genereic exception
        die(\Symfony\Component\VarDumper\VarDumper::dump($exception));
}