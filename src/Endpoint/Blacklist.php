<?php

declare(strict_types=1);

namespace Api\Client\Endpoint;

use Api\Client\HttpClient\Message\ResponseMediator;
use Api\Client\Sdk;
use Http\Client\Exception;
use Symfony\Component\VarDumper\VarDumper;

final class Blacklist
{
    /**
     * @var Sdk
     */
    private Sdk $sdk;
    
    /**
     * @param Sdk $sdk
     */
    public function __construct(Sdk $sdk)
    {
        $this->sdk = $sdk;
    }
    
    /**
     * @param $email
     * @return array
     * @throws Exception
     * @throws \Api\Client\HttpClient\Exception
     */
    public function get(string $email = null): array
    {
        if(!empty($email))
            $resp = $this->sdk->getHttpClient()->get("/blacklist/$email");
        else
            $resp = $this->sdk->getHttpClient()->get("/blacklist");
    
        if($resp->getStatusCode() !== 200)
            throw new \Api\Client\HttpClient\Exception($resp->getReasonPhrase(),$resp->getStatusCode());
        
        return ResponseMediator::getContent($resp);
    }
    
    
    public function add(string $email): array
    {
        $resp = $this->sdk->getHttpClient()->post("/blacklist/$email");
    
        if($resp->getStatusCode() !== 200)
            throw new \Api\Client\HttpClient\Exception($resp->getReasonPhrase(),$resp->getStatusCode());
    
        return ResponseMediator::getContent($resp);
    }
}